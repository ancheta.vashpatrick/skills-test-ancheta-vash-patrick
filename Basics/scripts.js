function validateForm() {
  let x = document.forms["sform"]["uname"].value;
  if (x == "") { // if username empty deny
    alert("Please input your username");
    return false;
  }
  let y = document.forms["sform"]["pword"].value;
  if (y == "") { // if password empty deny
    alert("Please input your password");
    return false;
  }

  if (x == "admin" && y == "admin") { // if inputted details are correct
    document.getElementById("rspns").innerHTML = "Successfully logged in!"; // state successful login
    document.getElementById("rspns").className = "valid"; // add class to change appearance
    return false; // true if actual login
  }
  else { // otherwise, inputted details are incorrect
    document.getElementById("rspns").innerHTML = "Invalid login details."; // state failed login
    document.getElementById("rspns").className = "invalid"; // add class to change appearance
    return false;
  }
}
